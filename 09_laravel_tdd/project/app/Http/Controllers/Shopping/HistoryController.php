<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\ShoppingList;
use App\Models\Product;
use App\Models\ProductFrequency;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class HistoryController extends Controller
{
    public function index()
    {
        $user_id = Auth::id();
        $listsCount = ShoppingList::where('user_id', $user_id)->where('done',true)->get()->count();
        if($listsCount>0) {
            $shoppingLists = ShoppingList::where('user_id', $user_id)->where('done',true)->latest()->get();
            return view('shopping.history.index')->withShoppingLists($shoppingLists);
        }
        else{
            return view('shopping.history.index')->withShoppingLists(NULL);
        }
    }

    public function show($shoppingListId)
    {
        $shoppingList = ShoppingList::find($shoppingListId);
        return view('shopping.history.show')->withShoppingList($shoppingList);
    }

    public function destroy($shoppingListId)
    {
        $shoppingList = ShoppingList::find($shoppingListId);
        foreach ($shoppingList->products as $product) {
            $user_lists = Auth::user()->lists;
            $user_lists = $user_lists->map( function($value, $key) use ($product){
                $collection = $value->products->where('product', $product->product);
                return $collection;
            });
            if($user_lists->flatten()->count() < 2) {
                $product_frequency = ProductFrequency::where('user_id', Auth::id())->where('product', $product->product)->first();
                $product_frequency->delete();
                if(!ProductFrequency::where('product', $product->product)->first())
                {
                    $product->delete();
                }
            }
        }
        $shoppingList->delete();

        return redirect()->route('history.index');
    }
}





