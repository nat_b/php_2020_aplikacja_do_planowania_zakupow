<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight bg-green-100 rounded-lg">
            {{ __('Shopping history') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-green-100 overflow-hidden shadow-sm sm:rounded-lg">

                @if($shoppingLists == NULL)
                    <p class="p-6">You have no shopping lists.</p>
                @else
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-green-100">
                        <tr>
                            <th scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                Created at
                            </th>

                            <th scope="col" class="relative px-6 py-3">
                                <span class="sr-only">Details</span>
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-green-50 divide-y divide-gray-200">
                        @foreach($shoppingLists as $shoppingList)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-500">{{ $shoppingList->created_at }}</div>
                                </td>

                                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                    <a href="{{ route('history.show',  $shoppingList ) }}" class="text-indigo-600 hover:text-indigo-900">Details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif


            </div>
        </div>
    </div>
</x-app-layout>
