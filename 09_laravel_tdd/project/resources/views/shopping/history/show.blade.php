<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight bg-green-100 rounded-lg">
            {{ __('Viewing a shopping list created at ' . $shoppingList->created_at) }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="bg-green-100 shadow overflow-hidden sm:rounded-lg">
                <div class="bg-green-100 px-4 py-5 pb-5 flex items-center justify-end mt-4 sm:grid sm:grid-cols-3 sm:px-6">

                    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-green-50 shadow-md overflow-hidden sm:rounded-lg">

                        <p class="p-6">Products:</p>
                        @foreach ($shoppingList->products as $product)
                            <li>
                                {{ $product->product }}
                            </li>
                        @endforeach

                    </div>

                    <form method="post" action="{{ route('history.destroy', $shoppingList) }}">

                        @csrf
                        @method("DELETE")

                        <x-button class="ml-4">
                            {{ __('Delete') }}
                        </x-button>
                    </form>

                </div>
            </div>

        </div>
    </div>

</x-app-layout>
