<x-app-layout>
    <meta name = "crf-token" content="{{csrf_token()}}">
    <script>src="{{asset('js/app.js')}}"</script>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight bg-green-100 rounded-lg">
            {{ __('My shopping list') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-2xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-green-100 overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                            {{ __('Shopping list') }}
                        </h3>
                        <div class="bg-green-100 px-4 pb-5 flex justify-end mt-4">
                        </div>
                        @if(empty($shoppingList) || $shoppingList->done == true)
                        <p class="mt-1 max-w-2xl text-sm text-gray-500">

                                You have no current shopping list! Create One!

                        </p>
                            <div class="bg-green-100 border-t border-gray-200">
                                <form action="{{route('shoppingList.create')}}" method="get">
                                    <div class="bg-green-100 px-4 pb-5 flex items-center justify-end mt-4">
                                        <x-button class="ml-4">
                                            {{ __('CREATE MY LIST') }}
                                        </x-button>
                                    </div>
                                </form>
                                <form action="{{route('generate.index')}}" method="get">
                                    <div class="bg-green-100 px-4 pb-5 flex items-center justify-end mt-4">
                                        <x-button class="ml-4">
                                            {{ __('GENERATE MY LIST') }}
                                        </x-button>
                                    </div>
                                </form>
                            </div>
                        @else
                            <form action="{{route('shoppingList.update',$shoppingList)}}" method="post">
                            <table>
                            <tbody class="bg-green-100 divide-y divide-gray-200">


                                @csrf
                                @method('PUT')
                                @foreach($shoppingList->products as $product)
                                <tr>
                                    <td class="bg-green-100 px-6 py-4 whitespace-nowrap">
                                    @if($boughtItems->contains(function($item,$key)use($product){
                                        return $item->product == $product->product;
                                        }))
                                        <td><x-label for="{{$product->id}}" ><del>{{$product->product}}</del></x-label></td>
                                    @else
                                        <td><x-label for="{{$product->id}}" >{{$product->product}}</x-label></td>
                                        <td><x-input id="{{$product->id}}" style="margin-left: 10px" type="checkbox" name="checkbox{{$product->id}}" :value="old('{{$product->id}}')"></x-input></td>

                                    @endif
                                </tr>

                                @endforeach
                            </tbody>
                            </table>
                <div class="bg-green-100 px-4 pb-5 flex justify-end mt-4">
                            <table>
                                <tbody class="bg-green-100 divide-y divide-gray-200">
                                <td class="bg-green-100 px-1  pb-5">
                                <td><x-button class="ml-4">
                                    {{ __('UPDATE LIST') }}
                                </x-button></td>

                                </tbody>
                            </table>
                </div>
                        </form>
                <div class="bg-green-100 px-4 pb-5 flex justify-end mt-4">
                    <table>
                        <tbody class="bg-green-100 divide-y divide-gray-200">
                        <form action="{{route('shoppingList.product.create',$shoppingList)}}" method="get">
                            <div class="bg-green-100 px-4 pb-5 flex justify-end mt-4">
                                <td><x-button class="ml-4">
                                    {{ __('ADD PRODUCT') }}
                                </x-button></td>
                            </div>
                        </form>

            </tbody>
            </table>
                </div>
            </div>
                @endif

            </div>
        </div>
    </div>
</x-app-layout>







