<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/comments', App\Http\Controllers\CommentController::class);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::resource('/history', App\Http\Controllers\Shopping\HistoryController::class)->middleware('auth');

Route::resource('/generate', App\Http\Controllers\Shopping\GenerateController::class)->middleware('auth');

Route::resource('/shoppingList', App\Http\Controllers\Shopping\ShoppingListController::class)->middleware('auth');
Route::resource('shoppingList.product',App\Http\Controllers\Shopping\ProductController::class)->middleware('auth');
